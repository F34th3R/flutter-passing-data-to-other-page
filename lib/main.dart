import 'package:flutter/material.dart';
import 'package:flutter_passing_datatonextpage/views/home_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
    @override
  Widget build(BuildContext context) {
        return new MaterialApp(
            debugShowCheckedModeBanner: false,
            title: "Passing data to the next page",
            theme: new ThemeData(
                primaryColor: Colors.blue,
            ),
            home: new HomePage(),
        );
  }
}
