import 'package:flutter/material.dart';
import 'package:flutter_passing_datatonextpage/views/home/next_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() {
    return new _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
    var _textController = new TextEditingController();
    @override
      Widget build(BuildContext context) {
        // TODO: implement build
        return new Scaffold(
            appBar: new AppBar(
                title: new Text("Demo Home"),
            ),
            body: new ListView(
                children: <Widget>[
                    new ListTile(
                        title: new TextField(
                            controller: _textController,
                        ),
                    ),
                    new ListTile(
                        title: new RaisedButton(
                            onPressed: () {
                                var route = new MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                    new NextPage(value: _textController.text,),
                                );
                                Navigator.of(context).push(route);
                            },
                            child: new Text("Next"),
                        ),
                    )
                ],
            ),
        );
      }
}