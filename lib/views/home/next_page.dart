import 'package:flutter/material.dart';

class NextPage extends StatefulWidget {
  final String value;

  NextPage({Key key, this.value}) : super (key: key);

  @override
  _NextPageState createState() => _NextPageState();
}

class _NextPageState extends State<NextPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Next Page"),
      ),
      body: new Center(
        child: new Text("${widget.value}"),
      ),
    );
  }
}
